from tkinter import *
import math

def button_click(value):
    current = resultado.get()
    if current == "0":
        resultado.set(value)
    else:
        resultado.set(current + value)

def clear():
    resultado.set("0")

def calculate():
    try:
        result = eval(resultado.get())
        resultado.set(result)
    except Exception as e:
        resultado.set("Error")

def ultimoCaracter():
    var = resultado.get()
    resultado.set(var[:-1])
    if resultado.get() == "":
        resultado.set("0")

def cuadrado():
    raiz = eval(resultado.get())
    result = math.sqrt(raiz)
    resultado.set(result)

ventana = Tk()
ventana.title("Calculadora")
ventana.geometry("+400+70")
ventana.iconbitmap("icono.ico")
ventana.config(bg="white", cursor="mouse")
ventana.resizable(0,0)

resultado = StringVar()
resultado.set("0")

frame = Frame(ventana, bg='#DBDBDB')
frame.grid(row=0, column=0)

entry = Entry(frame, textvariable=resultado, font=("Arial", 20), justify="right")
entry.grid(row=0, column=0, columnspan=4, sticky="nsew")

botonC = Button(frame, text="C", font="Arial 22", width=5, background="#CDCDCD", relief="flat", command=clear)
botonC.grid(row=1,column=0, sticky="nsew")

botonRaiz = Button(frame, text="√", font="Arial 22", width=5, background="#CDCDCD", relief="flat", command=cuadrado)
botonRaiz.grid(row=1, column=1, sticky="nsew")

botonDividir = Button(frame, text="/", font="Arial 22", width=5, background="#CDCDCD", relief="flat", command=lambda: button_click("/"))
botonDividir.grid(row=1, column=2, sticky="nsew")

botonCE = Button(frame, text="CE", font="Arial 22", width=5, background="#CDCDCD", relief="flat", command=ultimoCaracter)
botonCE.grid(row=1, column=3, sticky="nsew")

botonSiete = Button(frame, text="7", font="Arial 22", width=5, background="silver", relief="flat", command=lambda: button_click("7"))
botonSiete.grid(row=2, column=0, sticky="nsew")

botonOcho = Button(frame, text="8", font="Arial 22", width=5, background="silver", relief="flat", command=lambda: button_click("8"))
botonOcho.grid(row=2, column=1, sticky="nsew")

botonNueve = Button(frame, text="9", font="Arial 22", width=5, background="silver", relief="flat", command=lambda: button_click("9"))
botonNueve.grid(row=2, column=2, sticky="nsew")

botonMultiplicar = Button(frame, text="X", font="Arial 22", width=5, background="#CDCDCD", relief="flat", command=lambda: button_click("*"))
botonMultiplicar.grid(row=2, column=3, sticky="nsew")

botonCuatro = Button(frame, text="4", font="Arial 22", width=5, background="silver", relief="flat", command=lambda: button_click("4"))
botonCuatro.grid(row=3, column=0, sticky="nsew")

botonCinco = Button(frame, text="5", font="Arial 22", width=5, background="silver", relief="flat", command=lambda: button_click("5"))
botonCinco.grid(row=3, column=1, sticky="nsew")

botonSeis = Button(frame, text="6", font="Arial 22", width=5, background="silver", relief="flat", command=lambda: button_click("6"))
botonSeis.grid(row=3, column=2, sticky="nsew")

botonRestar = Button(frame, text="-", font="Arial 22", width=5, background="#CDCDCD", relief="flat", command=lambda: button_click("-"))
botonRestar.grid(row=3, column=3, sticky="nsew")

botonUno = Button(frame, text="1", font="Arial 22", width=5, background="silver", relief="flat", command=lambda: button_click("1"))
botonUno.grid(row=4, column=0, sticky="nsew")

botonDos = Button(frame, text="2", font="Arial 22", width=5, background="silver", relief="flat", command=lambda: button_click("2"))
botonDos.grid(row=4, column=1, sticky="nsew")

botonTres = Button(frame, text="3", font="Arial 22", width=5, background="silver", relief="flat", command=lambda: button_click("3"))
botonTres.grid(row=4, column=2, sticky="nsew")

botonSumar = Button(frame, text="+", font="Arial 22", width=5, background="#CDCDCD", relief="flat", command=lambda: button_click("+"))
botonSumar.grid(row=4, column=3, sticky="nsew")

botonCero = Button(frame, text="0", font="Arial 22", width=5, background="#CDCDCD", relief="flat", command=lambda: button_click("0"))
botonCero.grid(row=5, column=0, columnspan=2, sticky="nsew")

botonPunto = Button(frame, text=".", font="Arial 22", width=5, background="#CDCDCD", relief="flat", command=lambda: button_click("."))
botonPunto.grid(row=5, column=2, sticky="nsew")

botonIgual = Button(frame, text="=", font="Arial 22", width=5, background="#CDCDCD", relief="flat", command=calculate)
botonIgual.grid(row=5, column=3, sticky="nsew")

for child in frame.winfo_children():
    child.grid_configure(ipady=10, padx=1, pady=1)

ventana.mainloop()